import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;

public class ElfDetector {

    public Scanner loadELF(String path){


        try {
            File inputFile = new File(path);
            InputStream mInputStream = new FileInputStream(inputFile);
            return new Scanner(mInputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String args[]){

        ElfDetector mElfDetector = new ElfDetector();
        Scanner mScanner = mElfDetector.loadELF(args[0]);

        /*String[] woSpaces = mScanner.nextLine().split(" ");

        for(String s: woSpaces){
            System.out.println(s);
            System.out.println("\n\n");
        }*/

        System.out.println("Is the file passed as input an ELF executable? ");
        System.out.println(mElfDetector.isELFFile(mScanner));

    }

    private boolean isELFFile(Scanner scanner) {
        return scanner.nextLine().contains("ELF");
    }

}
