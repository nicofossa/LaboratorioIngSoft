#Lezione 1

## Divisione in ore
12 ore di esercitazione
* Toolset
* GUI Swing/JavaFX, Socket RMI, Junit

32 Ore di laboratorio
* Revisioni di progetto

Tool Ufficiali
	* Intellij
	* Git
	* Maven
	* Junit
	* Sonar => Importante, da caricare screenshot una volta ogni tanto su git

#Esercitazione 1

	* Versioning
	* Librerie java per scrivere su disco

##Versioning

Elementi fondamentali:

	* Working tree - file del progetto come sul disco fisso
	* Repository - l'insieme di tutta la storia delle versioni precedenti
	* Index - ...
	* Riferimenti - ...

La storia del progetto è rappresentata da un grafo aciclico.
Tag servono per identificare un certo commit es "v1.0".
Col termine branch ci si riferisce a dei puntatori a un particolare commit dal quale si proseguirà lo sviluppo.

## Comandi base di git (Locale)
### Inizializza repo
`git init`
### Inizializzare una repo dal server
`git clone`
### Checkout
`git checkout ` => cambia branch
### Agire sui file
`git add/rm/mv`
Esempio:
```
	git init

	touch pippo
	git add pippo
```

	(... modifiche)

```
	git add pippo
	git commit -m "commit message" // rendo permanenti le modifiche
	git push -u origin master

```

```
	git log // visualizza lo stato del repository => viene dato un hash del commit (identificativo)

```

### Branching
`git checkout -b <nomeBranch>`
Con questo comando ci si muove fra i vari branch, dove il branch "master" è quello principale
Si possono fare in vari modi, quello più intelligente è per task, ogni sviluppatore fa un branch per una specifica funzionalità e poi si fa il merge.

### Merging
`git merge <nomeBranch>` si "fondono" due branch.

## Comandi base di git (server)

### Fetching
`git fetch`
Scarica le modifiche sul locale, non integrandole

### Pulling
`git pull`
Prende le modifiche integrandole (Unendole) nel server locale

### Pushing
`git push -u <nomeBranch>`
Invia le modifiche al server remoto

### Conflitto
Quando due persone modificano lo stesso file, partendo dallo stesso commit
Le modifiche sono fatte per essere applicate su uno stato, se non si è più in quello stato l'operazione non va a buon fine e lo sviluppatore deve risincronizzare le modifche e integrare di nuovo.

## Quali file dovrebbero essere versionari?
Solitamente si preferisce versionare il codice sorgente, non i binari compilati, perchè non riesco a fare il merge **NON RISOLVI I CONFLITTI MANCO SE CI METTI NA CROCE SOPRA**
## .gitignore
Vanno specificati tutti i file da far ignorare a git

## Consigli di sviluppo

* Tanti piccoli commit is better than un commit grosso
* Committare spesso, documentando ogni commit in modo serio ed in maniera esplicativa
* Il codice committato deve funzionare, non solo compilare ma anche passare i tests


*NB per tornare ad un commit precedente prendere l'hash del file e `git checkout <hash>`, bastano anche solo i primi sette caratteri*

*NB 5 Giugno Ultima lezione => Valutazione a Luglio*
