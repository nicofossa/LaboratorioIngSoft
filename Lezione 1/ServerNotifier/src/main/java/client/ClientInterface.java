package client;

import java.rmi.RemoteException;

public interface ClientInterface {

    public void notify(String obj) throws RemoteException;

}
