package server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class Server {

    public static final int PORT = 1099;

    public static void main(String args[]){
        try {
            LocateRegistry.createRegistry(PORT);
        } catch (RemoteException e) {
            System.out.println("Registry already active on port " + PORT);
        }
    }

}
