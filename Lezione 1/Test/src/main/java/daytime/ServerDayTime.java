package daytime;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerDayTime {

    private ServerSocket serverSocket;
    private static final int PORT = 12345;

    public ServerDayTime() throws IOException {
        this.serverSocket = new ServerSocket(PORT);
    }

    public void run(){

        while(true){
            try {
                Socket socket = serverSocket.accept();
                new ClientConnection(socket).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
