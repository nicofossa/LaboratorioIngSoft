package daytime;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Date;

public class ClientConnection extends Thread {

    private Socket socket;

    public ClientConnection(Socket socket) {
        this.socket = socket;
    }

    public void run() {
        try {

            OutputStreamWriter mOutputStreamWriter = new OutputStreamWriter(socket.getOutputStream());
            mOutputStreamWriter.write(new Date().toString());
            mOutputStreamWriter.flush();
            mOutputStreamWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]){
        try {
            ServerDayTime server = new ServerDayTime();
            server.run();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
